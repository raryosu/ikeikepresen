# Require any additional compass plugins here.

# Set this to the root of your project when deployed:

http_path       = "/"
css_dir         = "dev/assets/css/"
sass_dir        = "src/css/"
images_dir		  = "../img/"
javascripts_dir	= "../js/"
line_comments	  = false
output_style	  = :expanded

additional_import_paths = [
	"src/honoka/src/compass/css/",
	"src/bootstrap/assets/stylesheets/"
]

